# CHANGELOG

<!--- next entry here -->

## 1.2.19
2021-04-16

### Fixes

- **ci:** remove duplicate change entry (5819f135afa5ead264d11a58e7dd7b46dcd8914b)

## 1.2.18
2021-04-16

### Fixes

- **ci:** show changelog (40358575fefd01b8fcb4f76380adda49130738d1)

## 1.2.18
2021-04-16

### Fixes

- **ci:** show changelog (40358575fefd01b8fcb4f76380adda49130738d1)

### Other changes

- adding changlog before build (09b5445f58ccc85067476ccd779ae4a2a080a660)
- Merge remote-tracking branch 'origin/master' (d4408f1407dd2206f61a0999d521f4cec390a6a9)

## 1.2.17
2021-03-27

### Fixes

- **ci:** add changelog output on version stage (10ce308f8b465abcc72837ccd14d5feb25cef8c2)
- **ci:** forgot to save changelog (ea6844f365e8ebee871442c35379f7fa17b35ea9)

## 1.2.16
2021-03-25

### Fixes

- **ci:** fix download (6b29925d1f9afe4d48c1d410978ff51579ca6639)

## 1.2.15
2021-03-25

### Fixes

- **ci:** missed it by that much (ab550e28e0e1f4f8195f4a8051817e84d532a2e1)

## 1.2.14
2021-03-25

### Fixes

- **ci:** damn variables (15ac52aca7ae59949117d33e314207b32664a4ac)

## 1.2.13
2021-03-25

### Fixes

- **ci:** testing some variables (68155d8430fd12e9e88f03119c8560117b5979e8)

## 1.2.12
2021-03-25

### Fixes

- **ci:** fix artifact job location (c7c2b3debde075c4b991946d32403734aa984f75)

## 1.2.11
2021-03-25

### Fixes

- **ci:** missing artifact (7c6540c56088c4485ca4b7ccf6a008bd95e8223a)

## 1.2.10
2021-03-25

### Fixes

- **ci:** use short sha (6cdfa7ca7f4ed53d2237388c2585d56ebd8ffcaf)

## 1.2.9
2021-03-25

### Fixes

- **ci:** change order of pub/rel (4285301feeece4ee0881d0a7c9838fe5ccba9af5)
- **ci:** bad copy command (3404d730f1250af1ece0b134a1b9f0e209720517)
- **ci:** bad filename (d9c07b8c79d0307bf8af353d018e796907811e10)
- **ci:** build name missed (d10d4077aa391bf73ff380bdb829da0cc055fbb7)

## 1.2.8
2021-03-25

### Fixes

- **ci:** list other changes (99bcd14cede7b3b92165ed7cc345e14bb061c483)

## 1.2.7
2021-03-25

### Fixes

- **stuff:** fix fix fix (7c49d16eaad47883803ab49d4f13c73df8b9867b)

### Other changes

- add option to show other changes (d2fd8c539bbf4b7002d20488d919d398958e00b6)
- Merge remote-tracking branch 'origin/master' (89037735ef9b927dce71c4a42e63bc3e4b06fa18)
- another change (14a54e6f17398053e8e4ffe63d21cc8504f9e613)
- updated semrel version (e9ea31101c3b236058069eee0598b2b24dc05955)
- trying other changes again (3e7b8a330db5b484aaee0058df45974ee260c307)

## 1.2.6
2021-03-25

### Fixes

- **ci:** publich attempt 2 (efdb6f0d4f8d6e12b33d4178494e579a888defcb)

## 1.2.5
2021-03-25

### Fixes

- **ci:** publish attempt 1 (4218c0a1d40f5c909e6808f94f3d52565527104b)

## 1.2.4
2021-03-25

### Fixes

- **ci:** silly did not need to install vsce (2bb4e2a62b9040b2e4cfc38c05c94b506bcbb918)

## 1.2.3
2021-03-25

### Fixes

- **ci:** ooops wrong install command (d7cd5d8352ba361f7ae60f7a7337bb1299d360b6)

## 1.2.2
2021-03-25

### Fixes

- **ci:** add vsce package (9502ae6b8055c91418db130f58b193f82363a4a7)

## 1.2.1
2021-03-25

### Fixes

- **ci:** add publish path (83b7841198ac73fae072351cab169be82229090b)
- **ci:** remove manual publish (8aba54290188d485ded95f3c1dc3b69941d680b0)
- **ci:** add environment dependency (9bb3bd7d7561e369818cdd10bb2e3475f368eace)
- **ci:** add publish stage (483052467805f44c39d13c9d5e0f9b898e1849e6)

## 1.2.0
2021-03-25

### Features

- new feature (8d7efa01192f05f8e1497983b0664adedbc3f687)

### Fixes

- small fix 4 (846575731666a23f90b249e370c6896721fe81c0)
- another small fix (f5e6c9479635b3fdc3893a93c3e08b9d1ecae244)
- final (1707ed754ed54a2101769164c438f697c71a5e03)

## 1.1.1
2021-03-24

### Fixes

- **file:** small fix (454ec1414e4497e7ae952903823d786dd68e0db7)

## 1.1.0
2021-03-16

### Features

- try again (40ac0c852253004947652bf26f1687b732743a7d)

### Fixes

- small change (55f25dbceb93eb2b66db45ec20cbc51a52dc5d89)
- small change (d2ceb0ab2c857e3a31c7dd83fb09c83dcc95b8d8)

## 1.0.19
2021-03-16

### Fixes

- change 1 (cadf693899613a0887687efd8e72a0f89af745f8)
- second fix (211fe7b66f4b8d4a8189e71c280ed68712d8bc75)
- third change (4b0ce2581188a70e873de85847d2f601911fc5f2)
- fourth change (025af160b711f099ed6e4e7534e163e2cc900d38)

## 1.0.18
2021-03-16

### Fixes

- release name (bd997ad0772eb514e2a7958470823e8906ef21b2)

## 1.0.17
2021-03-16

### Fixes

- missing name (f4a12a2a53b7123e49a606dd30695fd46e20fdd2)

## 1.0.16
2021-03-16

### Fixes

- i guess i needed the description (06e62f0b848f744fefcb1cf0a203090cb243b2a5)

## 1.0.15
2021-03-16

### Fixes

- extra quote in artifact (d59fba68478e6b3a0c46c71955bc3e0c41ea03d1)

## 1.0.14
2021-03-16

### Fixes

- added other stuff (1ee6cf5f90cfd05b1593aae59bbd09b26ce6bfe8)
- updated version of release (8c4a55797db014e52d79ab1f334ecd09da74eed5)
- remove release desc (601bff7b7659efe9b91f43e0a5ecd52a2d41ab62)

## 1.0.13
2021-03-16

### Fixes

- changed package file (dca73421c5b70ca7caea62540e4b11959cb27a8b)
- file changed (42f028052f26264827bec758ca9093e49dda1bea)

## 1.0.12
2021-03-16

### Fixes

- manual push (145d867a37442322555183351f5d470aed300897)

## 1.0.11
2021-03-16

### Fixes

- commit package.json (1253e990fbb5cfd86f5d06c09d41fad8fc0b54ea)

## 1.0.10
2021-03-16

### Fixes

- order of building env vars (be9fa13764268432ee2ef2d6bec575ab86f8da66)
- only on release (1e47fd705604e8b73a4e8b06620c62fb3faf69e8)

## 1.0.9
2021-03-15

### Fixes

- update release artifact (df6c7d2d985d10ab640167346e960c70f310a0b8)

## 1.0.8
2021-03-15

### Fixes

- trying something silly (aac1439c18e537ed1bb05b0684f30ea04db74bbc)
- restore vars from build info (3b1d9660be230a3d4d61941dbd625b1b471a05af)

## 1.0.7
2021-03-15

### Fixes

- save build info (f87306274f286c7e22bb67964d64e2de2a1fc5b8)

## 1.0.6
2021-03-15

### Fixes

- added release url (5b878b3d3af747f7c13068ea9cb7786488f98262)

## 1.0.6
2021-03-15

### Fixes

- added release url (5b878b3d3af747f7c13068ea9cb7786488f98262)

## 1.0.5
2021-03-15

### Fixes

- test fix comment (d5ac269b25b9ddacf92db03b6d00e4688443c385)
- update version before packaging (d3ea01d95bc6701c505c44eb414a51b968b9350d)
- put version in env var to use (92a8273036ccf10db7cdc89071be1015b254424e)
- added release section (abbaa1b37e7d597ef870d064cad3a47faf3124a4)
- proper release (2eaea12ae918a289de8e719703fec5095b4cb486)
- added tag and commit (c4f84906706b22578356489293f65cbd8cccf9ae)

## 1.0.5
2021-03-15

### Fixes

- test fix comment (d5ac269b25b9ddacf92db03b6d00e4688443c385)
- update version before packaging (d3ea01d95bc6701c505c44eb414a51b968b9350d)
- put version in env var to use (92a8273036ccf10db7cdc89071be1015b254424e)
- added release section (abbaa1b37e7d597ef870d064cad3a47faf3124a4)
- proper release (2eaea12ae918a289de8e719703fec5095b4cb486)
- added tag and commit (c4f84906706b22578356489293f65cbd8cccf9ae)